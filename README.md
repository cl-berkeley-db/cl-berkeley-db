cl-berkeley-db
==============
## A Common Lisp wrapper to the Berkeley DB library

This is a Common Lisp wrapper to the Berkeley DB library. It uses cffi and trivial-garbage. It's been tested on sbcl but should work on any distribution. There are a couple of functions that need to be implemented to make it work on other common-lisp implementations. This library has a BSD license. Our main project page has an implementation [summary](https://common-lisp.net/project/cl-berkeley-db/) as well as the [api](https://common-lisp.net/project/cl-berkeley-db/api/). Both are included in the docs directory of the main project. 
